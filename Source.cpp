#include <iostream>
#include <ctime>
#define _CRT_SECURE_NO_WARNINGS  /*We use it to avoid compiler errors
                                   related to localtime().*/

using namespace std;


int main()
{
	int N = 3;
	int sum;
	//We use this data type as the return value
	//of the function time().
	time_t t;
	//Returning the current calendat time
	time(&t);
	//Return a pointer to this structure to find
	//the ramainder of the devision by N.
	int k = (localtime(&t)->tm_mday) % N;  
	const int n = 2;                       
	const int m = 2;
	int arr[n][m];
	cout << "Remainder of division: " << k << endl;

	for (int i = 0; i < n; i++)
	{
		sum = 0;
		for (int j = 0; j < m; j++)
		{
			arr[i][j] = i + j;
			cout << arr[i][j] << " ";
			if (i == k) sum += arr[k][j];
		}
		cout << endl;
	}
	cout << "Sum of row: " << sum ;
}